package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.model.Email;
import com.bicicletario.externo.model.NovoEmail;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class EmailServiceTests {
    @Autowired
    private EmailService emailService;
    @Mock
    NovoEmail novoEmail;
    @Test
    public void deveRetornarEmailComConteudoCorreto(){
        //set up
        when(novoEmail.getEmail()).thenReturn("emailTeste@gmail.com");
        when(novoEmail.getMensagem()).thenReturn("Alguma mensagem");
        when(novoEmail.getAssunto()).thenReturn("Algum Assunto");
        //verificação
        try{
            //ação
            Email retorno = emailService.enviarEmail(this.novoEmail);

            assertEquals( "emailTeste@gmail.com",retorno.getEmail());
            assertEquals( "Algum Assunto",retorno.getAssunto());
            assertEquals( "Alguma mensagem",retorno.getMensagem());
        }catch (Exception e){
            fail("Lançou um " + e.getClass().toString());
        }
    }
    @Test
    public void deveRetornarErroEmailComFormatoInvalido(){
        try {
            //set up
            when(novoEmail.getEmail()).thenReturn("algumemail.com.br");
            when(novoEmail.getMensagem()).thenReturn("Alguma mensagem");
            when(novoEmail.getAssunto()).thenReturn("Algum Assunto");
            //ação
            Email retorno = emailService.enviarEmail(this.novoEmail);
            fail("Não lançou erro de email invlálido");
        }catch (DadosInvalidosException e) {
            //verificação
            assertEquals("422", e.getErroEmailEmailInvalido().getCodigo());
            assertEquals("E-mail com formato inválido", e.getErroEmailEmailInvalido().getMensagem());
        }
    }

    @Test
    public void deveRetornarErroEmailComInexistente(){
        try {
            when(novoEmail.getEmail()).thenReturn("naoexiste@gmail.com");
            when(novoEmail.getMensagem()).thenReturn("Alguma mensagem");
            when(novoEmail.getAssunto()).thenReturn("Algum Assunto");
            //ação
            Email retorno = emailService.enviarEmail(this.novoEmail);
            fail("Não lançou erro de email inexistente");
        }catch (DadoInexistenteException e) {
            //verificação
            assertEquals("404", e.getErro().getCodigo());
            assertEquals("E-mail não existe", e.getErro().getMensagem());
        }
    }
}

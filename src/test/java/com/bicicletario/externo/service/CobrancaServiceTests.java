package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.Cobranca;
import com.bicicletario.externo.model.NovaCobranca;
import com.bicicletario.externo.model.StatusCobranca;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static com.bicicletario.externo.model.StatusCobranca.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CobrancaServiceTests {
    @Autowired
    private CobrancaService cobrancaService;
    @Mock
    Cobranca cobranca;
    @Mock
    NovaCobranca novaCobranca;

    @Test
    public void deveRetornarCobrancaAorealizarCobranca() {
        //set up
        when(novaCobranca.getValor()).thenReturn((double)1);
        when(novaCobranca.getCiclista()).thenReturn(0);
        try {
            //açao
            Cobranca retorno = cobrancaService.realizarCobranca(novaCobranca);
            //Verificação
            assertEquals(1, retorno.getValor());
            assertEquals(0, retorno.getCiclista());
            assertEquals((PAGA.name()),retorno.getStatus());

        }catch (Exception e){
            fail("Lançou exceçao do tipo: " + e.getClass().toString());
        }
    }

    @Test
    public void deveLancarExceçaoAoRealizarCobranca() {
        when(novaCobranca.getValor()).thenReturn((double)-1);
        when(novaCobranca.getCiclista()).thenReturn(0);
        try {
            //açao
            Cobranca retorno = cobrancaService.realizarCobranca(novaCobranca);
            fail("Não lançou exceção ao passar valor negativo");
        } catch (DadosInvalidosException e) {
            //Verificação
            assertEquals("422", e.getErroDadosInvalidos().getCodigo());
            assertEquals("Dados inválidos", e.getErroDadosInvalidos().getMensagem());
        }
    }
    @Test
    public void deveInserirCobrancaPendenteNaFilaDeCobrancas() {
        //set up
        when(novaCobranca.getValor()).thenReturn((double) 1);
        when(novaCobranca.getCiclista()).thenReturn(0);
        try {
            //açao
            Cobranca retorno = cobrancaService.incluirCobrancaNaFila(novaCobranca);
            //Verificação
            assertEquals(1, retorno.getValor());
            assertEquals(0, retorno.getCiclista());
            assertEquals((PENDENTE.name()),retorno.getStatus());
            assertTrue(cobrancaService.getFilaCobrancas().contains(retorno));
        } catch (Exception e) {
            fail("Lançou exceçao do tipo: " + e.getClass().toString());
        }
    }

    @Test
    public void deveLancarExceçaoAoIncluirCObrancaNaFila() {
        when(novaCobranca.getValor()).thenReturn((double)-1);
        when(novaCobranca.getCiclista()).thenReturn(0);
        try {
            //açao
            Cobranca retorno = cobrancaService.incluirCobrancaNaFila(novaCobranca);
            fail("Não lançou exceção ao passar valor negativo");
        } catch (DadosInvalidosException e) {
            //Verificação
            assertEquals("422", e.getErroDadosInvalidos().getCodigo());
            assertEquals("Dados inválidos", e.getErroDadosInvalidos().getMensagem());
        }
    }
    @Test
    public void deveRetornarCobrancaSeExistir(){
        // set up
        Queue<Cobranca> lista = new LinkedList<>();
        lista.add(new Cobranca(1, "teste", "teste", "teste", 1.0, 0));
        cobrancaService.setFilaCobrancas(lista);

        //ação
        try{
             Cobranca retorno = cobrancaService.getCobranca(1);
             assertEquals(1, retorno.getId());
         }catch(DadosInvalidosException e){
            fail("Teste lançou exceção quando deveria ter achado a cobraça");
        }
    }
@Test
    public void deveErroCobrancaInexistente(){
        // set up
        Queue<Cobranca> lista = new LinkedList<>();
        lista.add(new Cobranca(1, "teste", "teste", "teste", 1.0, 0));
        cobrancaService.setFilaCobrancas(lista);

        //ação
        try{
            Cobranca retorno = cobrancaService.getCobranca(0);
            fail("Teste deveria ter lançado exceçao de nao achar cobrança");
            assertEquals(1, retorno.getId());
        }catch(DadoInexistenteException e){
            assertEquals("404", e.getErroCobrancaInexistente().getCodigo());
            assertEquals("Cobrança inexistente", e.getErroCobrancaInexistente().getMensagem());
        }
    }

    //testes formatador
    @Test
    public void testFormatarData() {
        // Criar uma LocalDateTime específica para teste
        LocalDateTime dataTeste = LocalDateTime.of(2023, 1, 15, 12, 30);

        // Chamar o método formatarData
        String dataFormatada = formatarData(dataTeste);

        // Formatar a data manualmente para comparação
        ZonedDateTime dataZonada = dataTeste.atZone(ZoneOffset.UTC);
        String formatoDesejado = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatoDesejado);
        String dataManual = dataZonada.format(formatter);

        // Verificar se a saída do método é igual à formatação manual
        assertEquals(dataManual, dataFormatada);
    }

    @Test
    public void testFormatarDataComNull() {
        // Chamar o método com null
        assertThrows(NullPointerException.class, () -> formatarData(null));
    }

    private String formatarData(LocalDateTime data){
        if (data == null) {
            throw new NullPointerException("A data não pode ser nula");
        }

        // Adicionar informações de fuso horário (UTC)
        ZonedDateTime dataZonada = data.atZone(ZoneOffset.UTC);

        // Formatar a data e hora como string
        String formatoDesejado = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatoDesejado);
        String dataFormatada = dataZonada.format(formatter);
        return dataFormatada;
    }

}

package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.NovoCartaoDeCredito;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
@SpringBootTest
public class CartaoDeCreditoServiceTests {
    @Autowired
    CartaoDeCreditoService cartaoDeCreditoService;
    @Mock
    NovoCartaoDeCredito novoCartaoDeCredito;

    @Test
    public void deveRetornarTrue(){
        when(novoCartaoDeCredito.getNomeTitular()).thenReturn("Gabriel R. Mesquita");
        when(novoCartaoDeCredito.getNumero()).thenReturn("1111-1111-1111-1111");
        when(novoCartaoDeCredito.getValidade()).thenReturn("2023-12-31");
        when(novoCartaoDeCredito.getCcv()).thenReturn("123");

        try{
            boolean retorno = cartaoDeCreditoService.isValidoCartaoDeCredito(novoCartaoDeCredito);

            assertEquals(true, retorno);
        }catch (Exception e){
            fail("Lancou excecao " + e.getClass().toString());
        }
    }

    @Test
    public void deveLancarExcecaoDadosInvalidos(){
        when(novoCartaoDeCredito.getNomeTitular()).thenReturn("Gabriel R. Mesquita");
        when(novoCartaoDeCredito.getNumero()).thenReturn("1111-1111-1111-1111");
        when(novoCartaoDeCredito.getValidade()).thenReturn("2022-12-31");
        when(novoCartaoDeCredito.getCcv()).thenReturn("123");

        try{
            cartaoDeCreditoService.isValidoCartaoDeCredito(novoCartaoDeCredito);

            fail("deveria ter lançado exceção");
        }catch (DadosInvalidosException e){
            assertEquals("422", e.getErroDadosInvalidos().getCodigo());
            assertEquals("Dados inválidos", e.getErroDadosInvalidos().getMensagem());
        }
    }
}

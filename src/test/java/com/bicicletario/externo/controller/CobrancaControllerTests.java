package com.bicicletario.externo.controller;

import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.Cobranca;
import com.bicicletario.externo.model.Erro;
import com.bicicletario.externo.model.NovaCobranca;
import com.bicicletario.externo.service.CobrancaService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CobrancaControllerTests {
    @Mock
    CobrancaService cobrancaService;
    @InjectMocks
    CobrancaController cobrancaController;
    @Mock
    Erro erro;
    @Mock
    Cobranca cobranca;
    @Mock
    NovaCobranca novaCobranca;

    @Test
    public void deveRetornarOkECobranca(){
        //set up
        when(cobrancaService.realizarCobranca(any(NovaCobranca.class))).thenReturn(cobranca);
        //açao
        ResponseEntity<Object> retorno = cobrancaController.realizarCobranca(novaCobranca);
        //verificacao
        assertEquals(HttpStatus.OK, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Cobranca);
    }

    @Test
    public void deveRetornarCodigo422eErro(){
        //set up
        when(cobrancaService.realizarCobranca(any(NovaCobranca.class))).thenThrow(new DadosInvalidosException(erro));
            //açao
        ResponseEntity<Object> retorno = cobrancaController.realizarCobranca(novaCobranca);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Erro);
    }
    @Test
    public void deveRetornarOkECobrancaAoIncluirNaFila(){
        //set up
        when(cobrancaService.incluirCobrancaNaFila(any(NovaCobranca.class))).thenReturn(cobranca);
        //açao
        ResponseEntity<Object> retorno = cobrancaController.incluirCobrancaNaFila(novaCobranca);
        //verificacao
        assertEquals(HttpStatus.OK, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Cobranca);
    }

    @Test
    public void deveRetornarCodigo422eErroAoIncluirNaFila(){
        //set up
        when(cobrancaService.incluirCobrancaNaFila(any(NovaCobranca.class)))
                .thenThrow(new DadosInvalidosException(erro));
        //açao
        ResponseEntity<Object> retorno = cobrancaController.incluirCobrancaNaFila(novaCobranca);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Erro);
    }

    @Test
    public void deveRetornarOkECobrancaAoBuscarCobranca(){
        //set up
        when(cobrancaService.getCobranca(anyInt())).thenReturn(cobranca);
        //açao
        ResponseEntity<Object> retorno = cobrancaController.getCobranca(1);
        //verificacao
        assertEquals(HttpStatus.OK, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Cobranca);
    }

    @Test
    public void deveRetornarCodigo422eErroAoBuscarCobranca() {
        //set up
        when(cobrancaService.getCobranca(anyInt()))
                .thenThrow(new DadoInexistenteException(erro));
        //açao
        ResponseEntity<Object> retorno = cobrancaController.getCobranca(1);

        assertEquals(HttpStatus.NOT_FOUND, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Erro);
    }

    @Test
    public void deveRetornarListaCobrancasStatusOk(){
        //set up
        when(cobrancaService.processarCobrancasNaFila()).thenReturn(new ArrayList<>());
        //açao
        ResponseEntity<Object> retorno = cobrancaController.processarCobrancasEmFIla();
        //verificacao
        assertEquals(HttpStatus.OK, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof ArrayList<?>);
    }

    @Test
    public void deveRetornarCodigo422eErroAoProcessarCObrancasEmFila() {
        //set up
        when(cobrancaService.processarCobrancasNaFila())
                .thenThrow(new DadosInvalidosException(erro));
        //açao
        ResponseEntity<Object> retorno = cobrancaController.processarCobrancasEmFIla();

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Erro);
    }
}

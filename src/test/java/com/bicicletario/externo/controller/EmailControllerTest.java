package com.bicicletario.externo.controller;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.model.Email;
import com.bicicletario.externo.model.Erro;
import com.bicicletario.externo.model.NovoEmail;
import com.bicicletario.externo.service.EmailService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
@SpringBootTest
public class EmailControllerTest {
//    private EmailController emailController;
//    private EmailService emailService;
//    @BeforeEach
//    public void setUp(){
//        emailService = mock(EmailService.class);
//        emailController = new EmailController(emailService);
//    }

    @Mock
    EmailService emailService;
    @InjectMocks
    EmailController emailController;
    @Mock
    Erro erro;
    @Mock
    Email email;
    @Mock
    NovoEmail novoEmail;
    @Test
    public void deveRetornarOkeEmail() {
    // Mock do serviço de e-mail
        when(emailService.enviarEmail(any(NovoEmail.class)))
                .thenReturn(email);

        // Chamada ao método do controlador
        ResponseEntity<Object> retorno = emailController.enviarEmail
                (novoEmail);

        // Verificações
        assertEquals(HttpStatus.OK, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Email);
    }
    @Test
    public void deveRetornarCodigo404eErro() {
        // Mock do serviço de e-mail
        when(emailService.enviarEmail(any(NovoEmail.class)))
                .thenThrow(new DadoInexistenteException(erro));

        // Chamada ao método do controlador
        ResponseEntity<Object> retorno = emailController.enviarEmail(novoEmail);

        // Verificações
        assertEquals(HttpStatus.NOT_FOUND, retorno.getStatusCode());
        assertTrue(retorno.getBody() instanceof Erro);
    }
    @Test
    public void deveRetornarCodigo422eErro() {
        // Mock do serviço de e-mail
        when(emailService.enviarEmail(any(NovoEmail.class)))
                .thenThrow(new DadosInvalidosException(erro));

        // Chamada ao método do controlador
        ResponseEntity<Object> response = emailController.enviarEmail(novoEmail);

        // Verificações
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
        assertTrue(response.getBody() instanceof Erro);
    }
}
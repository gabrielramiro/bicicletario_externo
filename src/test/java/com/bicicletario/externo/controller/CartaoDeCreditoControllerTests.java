package com.bicicletario.externo.controller;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.Erro;
import com.bicicletario.externo.model.NovoCartaoDeCredito;
import com.bicicletario.externo.service.CartaoDeCreditoService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
@SpringBootTest
public class CartaoDeCreditoControllerTests {
    @Mock
    CartaoDeCreditoService cartaoDeCreditoService;
    @Mock
    NovoCartaoDeCredito novoCartaoDeCredito;
    @Mock
    Erro erro;
    @InjectMocks
    CartãoDeCreditoController cartãoDeCreditoController;
    @Test
    public void deveRetornarOK(){
        //set up
        when(cartaoDeCreditoService.isValidoCartaoDeCredito(any(NovoCartaoDeCredito.class))).thenReturn(true);
            //açao
        ResponseEntity<Object> retorno = cartãoDeCreditoController.validarCartaoDeCredito(novoCartaoDeCredito);
            //verificacao
        assertEquals(HttpStatus.OK, retorno.getStatusCode());

    }

    @Test
    public void deveRetornarCodigo422eErro(){
        when(cartaoDeCreditoService.isValidoCartaoDeCredito(any(NovoCartaoDeCredito.class)))
                .thenThrow(new DadosInvalidosException(erro));
        //açao
        ResponseEntity retorno = cartãoDeCreditoController.validarCartaoDeCredito(novoCartaoDeCredito);
        //verificacao
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, retorno.getStatusCode());
    }
}

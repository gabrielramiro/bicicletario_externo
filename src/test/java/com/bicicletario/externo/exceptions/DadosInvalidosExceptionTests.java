package com.bicicletario.externo.exceptions;

import com.bicicletario.externo.model.Erro;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DadosInvalidosExceptionTests {
    @Test
    public void testConstrutorComErro() {
        Erro erro = new Erro();
        erro.setCodigo("422");
        erro.setMensagem("E-mail com formato inválido");

        DadosInvalidosException exception = new DadosInvalidosException(erro);

        assertEquals(erro, exception.getErroEmailEmailInvalido());
    }

    @Test
    public void testGetErroEmailEmailInvalido() {
        DadosInvalidosException exception = new DadosInvalidosException(new Erro());
        Erro erro = exception.getErroEmailEmailInvalido();

        assertEquals("422", erro.getCodigo());
        assertEquals("E-mail com formato inválido", erro.getMensagem());
    }

    @Test
    public void testGetErroDadosInvalidos() {
        DadosInvalidosException exception = new DadosInvalidosException(new Erro());
        Erro erro = exception.getErroDadosInvalidos();

        assertEquals("422", erro.getCodigo());
        assertEquals("Dados inválidos", erro.getMensagem());
    }
}

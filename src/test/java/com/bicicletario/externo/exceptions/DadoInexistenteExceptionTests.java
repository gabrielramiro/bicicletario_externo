package com.bicicletario.externo.exceptions;

import com.bicicletario.externo.model.Erro;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DadoInexistenteExceptionTests {
    @Test
    public void testConstrutorComErro() {
        Erro erro = new Erro();
        erro.setCodigo("404");
        erro.setMensagem("E-mail não existe");

        DadoInexistenteException exception = new DadoInexistenteException(erro);

        assertEquals(erro, exception.getErro());
    }

    @Test
    public void testGetErro() {
        DadoInexistenteException exception = new DadoInexistenteException(new Erro());
        Erro erro = exception.getErro();

        assertEquals("404", erro.getCodigo());
        assertEquals("E-mail não existe", erro.getMensagem());
    }

    @Test
    public void testGetErroCobrancaInexistente() {
        DadoInexistenteException exception = new DadoInexistenteException(new Erro());
        Erro erro = exception.getErroCobrancaInexistente();

        assertEquals("404", erro.getCodigo());
        assertEquals("Cobrança inexistente", erro.getMensagem());
    }
}

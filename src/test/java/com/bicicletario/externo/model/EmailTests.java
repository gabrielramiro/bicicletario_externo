package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class EmailTests {

    @Test
    public void testGetterSetterId() {
        Email email = new Email();
        email.setId(1);
        assertEquals(1, email.getId());
    }

    @Test
    public void testGetterSetterEmail() {
        Email email = new Email();
        email.setEmail("test@example.com");
        assertEquals("test@example.com", email.getEmail());
    }

    @Test
    public void testGetterSetterAssunto() {
        Email email = new Email();
        email.setAssunto("Teste de Assunto");
        assertEquals("Teste de Assunto", email.getAssunto());
    }

    @Test
    public void testGetterSetterMensagem() {
        Email email = new Email();
        email.setMensagem("Conteúdo da mensagem");
        assertEquals("Conteúdo da mensagem", email.getMensagem());
    }

    @Test
    public void testNotEquals() {
        Email email1 = new Email();
        email1.setId(1);

        Email email2 = new Email();
        email2.setId(2);

        assertNotEquals(email1, email2);
    }


    @Test
    public void testMetodoQueUsaOutraClasse() {
        // Exemplo de teste usando Mockito para simular o comportamento de outra classe
        OutraClasseMock outraClasseMock = mock(OutraClasseMock.class);
        when(outraClasseMock.metodoExterno()).thenReturn("Mocked result");

        Email email = new Email();
        email.setId(1);
        email.setAssunto(outraClasseMock.metodoExterno());

        assertEquals("Mocked result", email.getAssunto());
    }

    // Classe de mock para uso no último exemplo
    static class OutraClasseMock {
        String metodoExterno() {
            // Lógica do método original (não utilizada no teste)
            return "Result from original method";
        }
    }
}

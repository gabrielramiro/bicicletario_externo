package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
@SpringBootTest
public class CartaoDeCreditoTests {
    @Test
    public void testGetterSetterId() {
        CartaoDeCredito cartao = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        assertEquals(1, cartao.getId());
    }

    @Test
    public void testGetterSetterNomeTitular() {
        CartaoDeCredito cartao = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        assertEquals("Titular", cartao.getNomeTitular());
    }

    @Test
    public void testGetterSetterNumero() {
        CartaoDeCredito cartao = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        assertEquals("1234-5678-9012-3456", cartao.getNumero());
    }

    @Test
    public void testGetterSetterValidade() {
        CartaoDeCredito cartao = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        assertEquals("12/24", cartao.getValidade());
    }

    @Test
    public void testGetterSetterCvv() {
        CartaoDeCredito cartao = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        assertEquals("123", cartao.getCvv());
    }

    @Test
    public void testNotEquals() {
        CartaoDeCredito cartao1 = new CartaoDeCredito(1, "Titular", "1234-5678-9012-3456", "12/24", "123");
        CartaoDeCredito cartao2 = new CartaoDeCredito(2, "Outro Titular", "1111-2222-3333-4444", "01/23", "456");

        assertNotEquals(cartao1, cartao2);
    }
}

package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class NovoEmailTests {

    @Test
    public void testGetterSetterEmail() {
        NovoEmail novoEmail = new NovoEmail();
        novoEmail.setEmail("test@example.com");
        assertEquals("test@example.com", novoEmail.getEmail());
    }

    @Test
    public void testGetterSetterAssunto() {
        NovoEmail novoEmail = new NovoEmail();
        novoEmail.setAssunto("Teste de Assunto");
        assertEquals("Teste de Assunto", novoEmail.getAssunto());
    }

    @Test
    public void testGetterSetterMensagem() {
        NovoEmail novoEmail = new NovoEmail();
        novoEmail.setMensagem("Conteúdo da mensagem");
        assertEquals("Conteúdo da mensagem", novoEmail.getMensagem());
    }
    @Test
    public void testNotEquals() {
        NovoEmail email1 = new NovoEmail();
        email1.setEmail("test1@example.com");
        email1.setAssunto("Teste de Assunto");
        email1.setMensagem("Conteúdo da mensagem");

        NovoEmail email2 = new NovoEmail();
        email2.setEmail("test2@example.com");
        email2.setAssunto("Teste de Assunto");
        email2.setMensagem("Conteúdo da mensagem");

        assertNotEquals(email1, email2);
    }
}

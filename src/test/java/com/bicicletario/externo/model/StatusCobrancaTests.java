/*
package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class StatusCobrancaTests {
    @Test
    public void testValoresEnum() {
        assertEquals("PENDENTE", StatusCobranca.PENDENTE.name());
        assertEquals("PAGA", StatusCobranca.PAGA.name());
        assertEquals("FALHA", StatusCobranca.FALHA.name());
        assertEquals("CANCELADA", StatusCobranca.CANCELADA.name());
        assertEquals("OCUPADA", StatusCobranca.OCUPADA.name());
    }

    @Test
    public void testValoresIguais() {
        assertEquals(StatusCobranca.PENDENTE, StatusCobranca.PENDENTE);
        assertEquals(StatusCobranca.PAGA, StatusCobranca.PAGA);
        assertEquals(StatusCobranca.FALHA, StatusCobranca.FALHA);
        assertEquals(StatusCobranca.CANCELADA, StatusCobranca.CANCELADA);
        assertEquals(StatusCobranca.OCUPADA, StatusCobranca.OCUPADA);
    }

    @Test
    public void testConversaoFromString() {
        assertEquals(StatusCobranca.PENDENTE, StatusCobranca.valueOf("PENDENTE"));
        assertEquals(StatusCobranca.PAGA, StatusCobranca.valueOf("PAGA"));
        assertEquals(StatusCobranca.FALHA, StatusCobranca.valueOf("FALHA"));
        assertEquals(StatusCobranca.CANCELADA, StatusCobranca.valueOf("CANCELADA"));
        assertEquals(StatusCobranca.OCUPADA, StatusCobranca.valueOf("OCUPADA"));
    }

    @Test
    public void testConversaoFromStringInvalida() {
        assertThrows(IllegalArgumentException.class, () -> StatusCobranca.valueOf("INVALIDO"));
    }

    @Test
    public void testConversaoToString() {
        assertEquals("PENDENTE", StatusCobranca.PENDENTE.toString());
        assertEquals("PAGA", StatusCobranca.PAGA.toString());
        assertEquals("FALHA", StatusCobranca.FALHA.toString());
        assertEquals("CANCELADA", StatusCobranca.CANCELADA.toString());
        assertEquals("OCUPADA", StatusCobranca.OCUPADA.toString());
    }
}
*/

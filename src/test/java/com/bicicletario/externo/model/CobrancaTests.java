package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class CobrancaTests {
    @Test
    public void testGetterSetterId() {
        Cobranca cobranca = new Cobranca();
        cobranca.setId(1);
        assertEquals(1, cobranca.getId());
    }

    @Test
    public void testGetterSetterStatus() {
        Cobranca cobranca = new Cobranca();
        cobranca.setStatus("Pendente");
        assertEquals("Pendente", cobranca.getStatus());
    }

    @Test
    public void testGetterSetterHoraSolicitacao() {
        Cobranca cobranca = new Cobranca();
        cobranca.setHoraSolicitacao("2023-01-01T12:00:00");
        assertEquals("2023-01-01T12:00:00", cobranca.getHoraSolicitacao());
    }

    @Test
    public void testGetterSetterHoraFinalizacao() {
        Cobranca cobranca = new Cobranca();
        cobranca.setHoraFinalizacao("2023-01-01T14:00:00");
        assertEquals("2023-01-01T14:00:00", cobranca.getHoraFinalizacao());
    }

    @Test
    public void testGetterSetterValor() {
        Cobranca cobranca = new Cobranca();
        cobranca.setValor(100.0);
        assertEquals(100.0, cobranca.getValor());
    }

    @Test
    public void testGetterSetterCiclista() {
        Cobranca cobranca = new Cobranca();
        cobranca.setCiclista(123);
        assertEquals(123, cobranca.getCiclista());
    }

    @Test
    public void testNotEquals() {
        Cobranca cobranca1 = new Cobranca();
        cobranca1.setId(1);

        Cobranca cobranca2 = new Cobranca();
        cobranca2.setId(2);

        assertNotEquals(cobranca1, cobranca2);
    }
}

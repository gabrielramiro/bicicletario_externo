package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class NovoCartaoDeCreditoTests {
    @Test
    public void testGetterSetterValor() {
        NovaCobranca novaCobranca = new NovaCobranca();
        novaCobranca.setValor(100.0);
        assertEquals(100.0, novaCobranca.getValor());
    }

    @Test
    public void testGetterSetterCiclista() {
        NovaCobranca novaCobranca = new NovaCobranca();
        novaCobranca.setCiclista(123);
        assertEquals(123, novaCobranca.getCiclista());
    }

    @Test
    public void testNotEquals() {
        NovaCobranca cobranca1 = new NovaCobranca();
        cobranca1.setValor(100.0);
        cobranca1.setCiclista(123);

        NovaCobranca cobranca2 = new NovaCobranca();
        cobranca2.setValor(200.0);
        cobranca2.setCiclista(456);

        assertNotEquals(cobranca1, cobranca2);
    }



}

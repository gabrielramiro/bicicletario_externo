package com.bicicletario.externo.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
@SpringBootTest
public class ErroTests {
    @Test
    public void testGetterSetterCodigo() {
        Erro erro = new Erro();
        erro.setCodigo("001");
        assertEquals("001", erro.getCodigo());
    }

    @Test
    public void testGetterSetterMensagem() {
        Erro erro = new Erro();
        erro.setMensagem("Erro de teste");
        assertEquals("Erro de teste", erro.getMensagem());
    }
    @Test
    public void testNotEquals() {
        Erro erro1 = new Erro();
        erro1.setCodigo("001");
        erro1.setMensagem("Erro de teste");

        Erro erro2 = new Erro();
        erro2.setCodigo("002");
        erro2.setMensagem("Outro erro de teste");

        assertNotEquals(erro1, erro2);
    }

}

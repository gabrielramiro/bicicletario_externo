package com.bicicletario.externo.controller;

import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.Cobranca;
import com.bicicletario.externo.model.NovaCobranca;
import com.bicicletario.externo.service.CobrancaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CobrancaController {
    @Autowired
    private CobrancaService cobrancaService;
    @PostMapping("/cobranca")
    public ResponseEntity<Object> realizarCobranca
            (@RequestBody NovaCobranca novaCobranca){
        try{
            Cobranca retorno = cobrancaService.realizarCobranca(novaCobranca);
            return ResponseEntity.ok(retorno);
        }catch (DadosInvalidosException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body(e.getErroDadosInvalidos());
        }
    }
    @PostMapping("/filaCobranca")
    public ResponseEntity<Object> incluirCobrancaNaFila
            (@RequestBody NovaCobranca novaCobranca){
        try{
            Cobranca retorno = cobrancaService.incluirCobrancaNaFila(novaCobranca);
            return ResponseEntity.ok(retorno);
        }catch (DadosInvalidosException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(e.getErroDadosInvalidos());
        }
    }

    @PostMapping("/processaCobrancasEmFila")
    public ResponseEntity<Object> processarCobrancasEmFIla(){
        try{
            List<Cobranca> retorno = cobrancaService.processarCobrancasNaFila();
            return ResponseEntity.ok(retorno);
        }catch (DadosInvalidosException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(e.getErroDadosInvalidos());
        }
    }

    @GetMapping("/cobranca/{idCobranca}")
    public ResponseEntity<Object> getCobranca(@PathVariable Integer idCobranca) {
        try{
            Cobranca retorno = cobrancaService.getCobranca(idCobranca);
            return ResponseEntity.ok(retorno);
        }catch (DadoInexistenteException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(((DadoInexistenteException) e).getErro());
        }
    }
}

package com.bicicletario.externo.controller;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.NovoCartaoDeCredito;
import com.bicicletario.externo.service.CartaoDeCreditoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class CartãoDeCreditoController {
    @Autowired
    CartaoDeCreditoService cartaoDeCreditoService;

    @PostMapping("/validaCartaoDeCredito")
    public ResponseEntity validarCartaoDeCredito
            (@RequestBody NovoCartaoDeCredito novoCartaoDeCredito){
        try{
            cartaoDeCreditoService.isValidoCartaoDeCredito(novoCartaoDeCredito);
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (DadosInvalidosException e){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(e.getErroDadosInvalidos());
        }
    }
}

package com.bicicletario.externo.controller;


import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.model.Email;
import com.bicicletario.externo.model.NovoEmail;
import com.bicicletario.externo.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {
    @Autowired
    private EmailService emailService;

//    public EmailController(EmailService emailService) {
//        this.emailService = emailService;
//    }

    @PostMapping("/enviarEmail")
    public ResponseEntity<Object> enviarEmail
            (@RequestBody NovoEmail novoEmail){
        try{
            Email retorno = emailService.enviarEmail(novoEmail);
                return ResponseEntity.ok(retorno);
        }catch (Exception e){
            if (e instanceof DadosInvalidosException)
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body(((DadosInvalidosException) e).getErroEmailEmailInvalido());
            else
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(((DadoInexistenteException) e).getErro());
        }
    }
}

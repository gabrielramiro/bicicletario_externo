package com.bicicletario.externo.exceptions;

import com.bicicletario.externo.model.Erro;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
@NoArgsConstructor
@Component
public class DadosInvalidosException extends RuntimeException {

    private Erro erro;
    public DadosInvalidosException(Erro erro){
        this.erro = erro;
    }

    public Erro getErroEmailEmailInvalido() {
        erro.setCodigo("422");
        erro.setMensagem("E-mail com formato inválido");
        return erro;
    }
    public Erro getErroDadosInvalidos(){
        erro.setCodigo("422");
        erro.setMensagem("Dados inválidos");
        return erro;
    }

}
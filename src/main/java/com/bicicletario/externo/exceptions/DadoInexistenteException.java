
package com.bicicletario.externo.exceptions;

import com.bicicletario.externo.model.Erro;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@NoArgsConstructor
@Component
public class DadoInexistenteException extends RuntimeException {
    @Autowired
    private Erro erro;
public DadoInexistenteException(Erro erro){
    this.erro = erro;
}
    public Erro getErro(){
        erro.setCodigo("404");
        erro.setMensagem("E-mail não existe");
        return erro;
    }

    public Erro getErroCobrancaInexistente() {
        erro.setCodigo("404");
        erro.setMensagem("Cobrança inexistente");
        return erro;
    }
}
package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.Erro;
import com.bicicletario.externo.model.NovoCartaoDeCredito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
@Service
public class CartaoDeCreditoService {
    @Autowired
    Erro erro;
    public Boolean isValidoCartaoDeCredito(NovoCartaoDeCredito novoCartaoDeCredito){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dataValidade = LocalDate.parse(novoCartaoDeCredito.getValidade(), formatter);

        LocalDate dataAtual = LocalDate.now();
        if(!novoCartaoDeCredito.getNomeTitular().isBlank()
                && novoCartaoDeCredito.getNumero().length() > 0
                && !dataValidade.isBefore(dataAtual)
                && (novoCartaoDeCredito.getCcv().length() == 3 || novoCartaoDeCredito.getCcv().length() == 4))
            return true;
        else
            throw new DadosInvalidosException(erro);
    }
}

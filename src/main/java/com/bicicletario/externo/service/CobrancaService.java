package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.model.*;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.bicicletario.externo.model.StatusCobranca.*;

@Service
public class CobrancaService {
    private Integer id;
    private static final AtomicInteger contador = new AtomicInteger(0);

    @Getter @Setter
    private Queue<Cobranca> filaCobrancas = new LinkedList<>();
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    CartaoDeCreditoService cartaoDeCreditoService;
    @Autowired
    NovoCartaoDeCredito novoCartaoDeCredito;
    @Autowired
    private Cobranca cobranca;
    @Autowired
    Erro erro;
    @Autowired
    NovaCobranca novaCobranca;
    public Cobranca realizarCobranca(NovaCobranca novaCobranca){
        //valida dados
        if (!isDadosCobrancaValidos(novaCobranca.getValor(),novaCobranca.getCiclista())){
            throw new DadosInvalidosException(erro);
        }
        // pegar cartao  -DESCOMENTAR ABAIXO QUANDO TIVER O ENDPOINT FUNCIONANDO
//        ResponseEntity<CartaoDeCredito> resposta = restTemplate.getForEntity
//                ("http://localhost:8081/cartaoDeCredito/" + novaCobranca.getCiclista().toString()
//                        ,CartaoDeCredito.class);
//        //settar novo cartao de credito para poder validar
//
//        novoCartaoDeCredito.setNomeTitular(resposta.getBody().getNomeTitular());
//        novoCartaoDeCredito.setNumero(resposta.getBody().getNumero());
//        novoCartaoDeCredito.setValidade(resposta.getBody().getValidade());
//        novoCartaoDeCredito.setCcv(resposta.getBody().getCvv());
//        //valida novo cartão de credito
//        if (!cartaoDeCreditoService.isValidoCartaoDeCredito(novoCartaoDeCredito)){
//            this.cobranca.setId(gerarID());
//            this.cobranca.setStatus(FALHA.name());
//            this.cobranca.setHoraSolicitacao(formatarData(LocalDateTime.now()));
//            this.cobranca.setHoraFinalizacao(formatarData(LocalDateTime.now()));
//            this.cobranca.setValor(novaCobranca.getValor());
//            this.cobranca.setCiclista(novaCobranca.getCiclista());
//            return  cobranca;
//        }
        this.cobranca.setId(gerarID());
        this.cobranca.setStatus(PAGA.name());
        this.cobranca.setHoraSolicitacao(formatarData(LocalDateTime.now()));
        this.cobranca.setHoraFinalizacao(formatarData(LocalDateTime.now()));
        this.cobranca.setValor(novaCobranca.getValor());
        this.cobranca.setCiclista(novaCobranca.getCiclista());
        return  cobranca;
    }


    public Cobranca incluirCobrancaNaFila(NovaCobranca novaCobranca){
        //valida dados
        if (!isDadosCobrancaValidos(novaCobranca.getValor(), novaCobranca.getCiclista()))
            throw new DadosInvalidosException(erro);
    else {
            this.cobranca.setId(gerarID());
            this.cobranca.setStatus(PENDENTE.name());
            this.cobranca.setHoraSolicitacao(formatarData(LocalDateTime.now()));
            this.cobranca.setHoraFinalizacao(formatarData(LocalDateTime.now()));
            this.cobranca.setValor(novaCobranca.getValor());
            this.cobranca.setCiclista(novaCobranca.getCiclista());
            filaCobrancas.add(cobranca);
            return this.cobranca;
        }
}
    public List processarCobrancasNaFila() {
        if (filaCobrancas.isEmpty())
            throw new DadosInvalidosException(erro);
        else {
            List<Cobranca> listaCobrancasProcessadas = new ArrayList<>();
            while (!filaCobrancas.isEmpty()) {
                cobranca = filaCobrancas.poll();
//        pegar cartao  -DESCOMENTAR ABAIXO QUANDO TIVER O ENDPOINT FUNCIONANDO
                ResponseEntity<CartaoDeCredito> resposta = restTemplate.getForEntity
                        ("http://localhost:8081/cartaoDeCredito/" + novaCobranca.getCiclista().toString()
                                , CartaoDeCredito.class);
                //settar novo cartao de credito para poder validar

                novoCartaoDeCredito.setNomeTitular(resposta.getBody().getNomeTitular());
                novoCartaoDeCredito.setNumero(resposta.getBody().getNumero());
                novoCartaoDeCredito.setValidade(resposta.getBody().getValidade());
                novoCartaoDeCredito.setCcv(resposta.getBody().getCvv());
                //valida novo cartão de credito
                if (!cartaoDeCreditoService.isValidoCartaoDeCredito(novoCartaoDeCredito)) {
                    cobranca.setStatus(PAGA.name());
                }
                else{
                    cobranca.setStatus(FALHA.name());
                }
                listaCobrancasProcessadas.add(cobranca);
            }
            return listaCobrancasProcessadas;
        }
    }

    public Cobranca getCobranca(Integer id){
        while (!this.filaCobrancas.isEmpty()){
            cobranca = filaCobrancas.poll();
            if (cobranca.getId() == id){
                return cobranca;
            }
        }
        throw new DadoInexistenteException(erro);
    }
    private boolean isDadosCobrancaValidos(Double valor, Integer ciclista){
        return (valor > 0
                && !(valor.isNaN())
                && ciclista >= 0
                && ciclista != null
                && ciclista instanceof Integer);
    }

    private Integer gerarID(){
        return this.id =  contador.incrementAndGet();
    }
    private String formatarData(LocalDateTime data){
        // Adicionar informações de fuso horário (UTC)
        ZonedDateTime dataZonada = data.atZone(ZoneOffset.UTC);

        // Formatar a data e hora como string
        String formatoDesejado = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatoDesejado);
        String dataFormatada = dataZonada.format(formatter);
        return dataFormatada;
    }
}

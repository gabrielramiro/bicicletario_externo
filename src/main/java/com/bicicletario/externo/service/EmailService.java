package com.bicicletario.externo.service;

import com.bicicletario.externo.exceptions.DadosInvalidosException;
import com.bicicletario.externo.exceptions.DadoInexistenteException;
import com.bicicletario.externo.model.Email;
import com.bicicletario.externo.model.Erro;
import com.bicicletario.externo.model.NovoEmail;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class EmailService {
    private static final AtomicInteger contador = new AtomicInteger(0);
    private Integer id;
    @Autowired
    private Email email;
    @Autowired
    Erro erro;
    public Email enviarEmail(@NotNull NovoEmail novoEmail){
        if (!isDadosEmailValidos(novoEmail.getEmail(), novoEmail.getMensagem()))
            throw new DadosInvalidosException(erro);
        else if (novoEmail.getEmail().contentEquals("naoexiste@gmail.com"))
            throw new DadoInexistenteException(erro);
        else{
            this.email.setId(gerarID());
            this.email.setEmail((novoEmail.getEmail()));
            this.email.setAssunto(novoEmail.getAssunto());
            this.email.setMensagem(novoEmail.getMensagem());
            return this.email;
        }
    }
    private Integer gerarID(){
       return this.id =  contador.incrementAndGet();
    }

    private boolean isDadosEmailValidos(String email, String mensagem){
        return (email.indexOf('@') != -1
                && !(email.isBlank())
                && !(email == null)
                && (email instanceof String)
                && (mensagem instanceof String)
                && !(mensagem.isBlank())
                && ! (mensagem == null));
    }
}

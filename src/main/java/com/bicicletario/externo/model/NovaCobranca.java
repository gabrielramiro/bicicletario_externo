package com.bicicletario.externo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
public class NovaCobranca {
    @Getter
    @Setter
    private Double valor;

    @Getter@Setter
    private Integer ciclista;
}

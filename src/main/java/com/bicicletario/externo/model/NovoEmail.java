package com.bicicletario.externo.model;

import lombok.Getter;
import lombok.Setter;

public class NovoEmail {
    @Getter@Setter
    private String email;

    @Getter@Setter
    private String assunto;

    @Getter@Setter
    private String mensagem;
}

package com.bicicletario.externo.model;

public enum StatusCobranca {
    PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA;
}

package com.bicicletario.externo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
public class NovoCartaoDeCredito {
    @Getter @Setter
    private String nomeTitular;
    @Getter @Setter
    private String numero;
    @Getter @Setter
    private String validade;
    @Getter @Setter
    private String ccv;

}

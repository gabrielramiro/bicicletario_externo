package com.bicicletario.externo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
//@AllArgsConstructor
@NoArgsConstructor
public class CartaoDeCredito {
    @Getter
    @Setter
    private Integer id;

    @Getter @Setter
    private String nomeTitular;

    @Getter @Setter
    private String numero;

    @Getter @Setter
    private String validade;

    @Getter @Setter
    private String cvv;


    public CartaoDeCredito(Integer id, String nomeTitular, String numero, String validade, String cvv) {
        this.id = id;
        this.nomeTitular = nomeTitular;
        this.numero = numero;
        this.validade = validade;
        this.cvv = cvv;
    }
}
